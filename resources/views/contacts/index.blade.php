@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="alert alert-info hide">Contact successfully deleted</div>
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>
                    <div class="panel-body">
                        You are logged in!
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Search</div>
                    <div class="panel-body">
                        {!! Form::open(array('url' => 'search', 'method' => 'get', 'id' => 'form-search', 'class' => 'form-horizontal' )) !!}
                        <div class="form-group">
                            <div class="col-sm-11">
                                {!! Form::text('q', '', array('id' => 'q', 'class' => 'form-control', 'placeholder' => 'Search')); !!}
                            </div>
                            <div class="col-sm-1">
                                <button class="btn btn-success" type="submit">Go</button>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Contacts</div>

                    <div class="panel-body">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-success btn-add" data-toggle="modal" data-target="#contact-modal">
                          Add Contact
                        </button>
                    </div>

                    <table class="table" id="contact-table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        @if (count($contacts) > 0)
                            <tbody>
                                @foreach ($contacts as $contact)
                                    <tr id="{{ $contact->id }}">
                                        <th scope="row">{{ $contact->id }}</th>
                                        <td>{{ $contact->first_name }}</td>
                                        <td>{{ $contact->last_name }}</td>
                                        <td>{{ $contact->email }}</td>
                                        <td>{{ $contact->phone }}</td>
                                        <td>
                                            <button type="button" class="btn btn-info btn-edit" data-toggle="modal" data-target="#contact-modal" data-id="{{ $contact->id }}">Edit</button>
                                            <a href="{{ url("contact/{$contact->id}") }}" class="btn btn-danger delete-contact" data-remote="true" data-method="delete" data-confirm="Are you sure you want to delete this contact">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        @endif
                    </table>

                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="contact-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Add Contact</h4>
          </div>
          <div class="modal-body">
            {!! Form::open(array('url' => 'contact', 'method' => 'post', 'id' => 'form-contact', 'class' => 'form-horizontal ajax-form', 'data-remote' => 'true' )) !!}
                {!! Form::hidden('id', '', array('id' => 'contact_id', 'type' => 'hidden')); !!}
                <div class="form-group">
                    <label for="first_name" class="col-sm-3 control-label">First Name</label>
                    <div class="col-sm-6">
                        {!! Form::text('first_name', '', array('id' => 'first_name', 'class' => 'form-control')); !!}
                    </div>
                </div>

                <div class="form-group">
                    <label for="last_name" class="col-sm-3 control-label">Last Name</label>
                    <div class="col-sm-6">
                        {!! Form::text('last_name', '', array('id' => 'last_name', 'class' => 'form-control')); !!}
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-6">
                        {!! Form::text('email', '', array('id' => 'email', 'class' => 'form-control')); !!}
                    </div>
                </div>

                <div class="form-group">
                    <label for="phone" class="col-sm-3 control-label">Phone</label>
                    <div class="col-sm-6">
                        {!! Form::text('phone', '', array('id' => 'phone', 'class' => 'form-control')); !!}
                    </div>
                </div>

                <div class="form-group">
                    <label for="custom-fields" class="col-sm-3 control-label">Custom Fields</label>
                    <div class="col-sm-6 custom-fields-container">
                        <input type="text" name="contact_fields[]" class="form-control">
                        <button type="button" class="btn btn-info btn-add-field">Add Field</button>
                    </div>
                </div>

            {{ Form::close() }}

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary btn-save-contact">Save Contact</button>
          </div>
        </div>
      </div>
    </div>

@endsection
