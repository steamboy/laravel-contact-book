@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Contacts</div>

                    <div class="panel-body">
                        <!-- Button trigger modal -->
                        {!! Form::open(array('url' => 'search', 'method' => 'get', 'id' => 'form-contact', 'class' => 'form-horizontal' )) !!}
                        <div class="form-group">
                            <div class="col-sm-11">
                                {!! Form::text('q', '', array('id' => 'q', 'class' => 'form-control', 'placeholder' => 'Search')); !!}
                            </div>
                            <div class="col-sm-1">
                                <button class="btn btn-success" type="submit">Go</button>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>

                    <table class="table" id="contact-table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        @if (count($contacts) > 0)
                            <tbody>
                                @foreach ($contacts as $contact)
                                    <tr id="{{ $contact->id }}">
                                        <th scope="row">{{ $contact->id }}</th>
                                        <td>{{ $contact->first_name }}</td>
                                        <td>{{ $contact->last_name }}</td>
                                        <td>{{ $contact->email }}</td>
                                        <td>{{ $contact->phone }}</td>
                                        <td>
                                            <button type="button" class="btn btn-info btn-edit" data-toggle="modal" data-target="#contact-modal" data-id="{{ $contact->id }}">Edit</button>
                                            <a href="{{ url("contact/{$contact->id}") }}" class="btn btn-danger delete-contact" data-remote="true" data-method="delete" data-confirm="Are you sure you want to delete this contact">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        @endif
                    </table>

                </div>
            </div>
        </div>
    </div>

@endsection
