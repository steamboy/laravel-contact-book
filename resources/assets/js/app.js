(function() {
  var FormHelper;

  FormHelper = FormHelper || {};

  this.FormHelper = {
    initialize: function() {},
    showValidationErrors: function(form, obj) {
      $('.form-group').removeClass('has-error');
      $('.help-block.has-error').remove();
      $.each(obj, function(id, error_messages) {
        var input, input_id, label;
        input_id = id;
        label = form.find("label[for='" + input_id + "']");
        if (!label.parent().hasClass('has-error')) {
          label.parent().addClass('has-error');
        }
        input = form.find("#" + input_id);
        if (label.parent().find('.radio').length > 0) {
          input = label.parent().find('.radio').last().find('input').parent().parent();
        }
        if (label.parent().find('.radio-inline').length > 0) {
          input = label.parent().find('.radio-inline').last().find('input').parent().parent();
        }
        return $.each(error_messages, function(index, error_message) {
          if (input.parent().find('span:contains("' + error_message + '")').length === 0) {
            return $('<span for="validation_name" class="help-block has-error">' + error_message + '</span>').insertAfter(input);
          }
        });
      });
    }
  };

}).call(this);

(function() {
  $(function() {
    var render_custom_input, render_row;
    $(".btn-save-contact").click(function() {
      console.log('save contact');
      $('#form-contact').submit();
    });
    $.ajaxSetup({
      dataType: 'json'
    });
    $('.ajax-form').on('ajax:success', function(e, data, status, xhr) {
      console.log('success');
      console.log(data);
      $('#contact-modal').modal('hide');
      render_row(data.contact, data.action_type);
    }).on("ajax:error", function(e, xhr, status, error) {
      var response_json;
      console.log('process error');
      response_json = JSON.parse(xhr.responseText);
      FormHelper.showValidationErrors($(".ajax-form"), response_json);
    });
    $(document).on("ajax:success", ".delete-contact", function(e, data, status, xhr) {
      console.log('success');
      $(this).closest('tr').remove();
    }).on("ajax:error", function(e, xhr, status, error) {
      console.log('process error');
    });
    $("#contact-modal").on("hidden.bs.modal", function(e) {
      var modal;
      console.log('remove data');
      modal = $(this);
      modal.removeData('bs.modal');
      modal.find('.modal-body #contact_id').val('');
      modal.find('.modal-body #first_name').val('');
      modal.find('.modal-body #last_name').val('');
      modal.find('.modal-body #email').val('');
      modal.find('.modal-body #phone').val('');
      modal.find('.modal-body .custom-fields-container input').remove();
      modal.find('.modal-body .custom-fields-container').prepend(render_custom_input());
    });
    $('#contact-modal').on('show.bs.modal', function(event) {
      var button, contact_id, modal, modal_title;
      modal = $(this);
      button = $(event.relatedTarget);
      if (button.hasClass('btn-add')) {
        modal_title = 'Add Contact';
        modal.find('.modal-body form').attr('method', 'post');
        modal.addClass('add-contact-modal');
      } else {
        contact_id = button.data('id');
        modal_title = 'Edit Contact';
        modal.find('.modal-body form').attr('method', 'put');
        modal.addClass('edit-contact-modal');
        $.get("/get_contact?id=" + contact_id, function(data) {
          var custom_inputs;
          console.log(data);
          modal.find('.modal-body #contact_id').val(data.contact.id);
          modal.find('.modal-body #first_name').val(data.contact.first_name);
          modal.find('.modal-body #last_name').val(data.contact.last_name);
          modal.find('.modal-body #email').val(data.contact.email);
          modal.find('.modal-body #phone').val(data.contact.phone);
          if (data.contact_fields.length > 0) {
            custom_inputs = [];
            $.each(data.contact_fields, function(key, value) {
              return custom_inputs.push(render_custom_input(value.name));
            });
            modal.find('.modal-body .custom-fields-container input').remove();
            modal.find('.modal-body .custom-fields-container').prepend(custom_inputs.join(''));
          }
        });
      }
      modal.find('.modal-title').text(modal_title);
    });
    $(".btn-add-field").click(function() {
      console.log('render custom field');
      $(render_custom_input()).insertAfter($('.custom-fields-container input').last());
      return $('.custom-fields-container input').last().focus();
    });
    render_row = function(contact, action_type) {
      var current_row, prev_row, row, table;
      row = "<tr id='" + contact.id + "'> <th scope='row'>" + contact.id + "</th> <td>" + contact.first_name + "</td> <td>" + contact.last_name + "</td> <td>" + contact.email + "</td> <td>" + contact.phone + "</td> <td> <button type='button' class='btn btn-info btn-edit' data-toggle='modal' data-target='#contact-modal' data-id='" + contact.id + "'>Edit</button> <a href='http://contactbook.dev/contact/" + contact.id + "' class='btn btn-danger delete-contact' data-remote='true' data-method='delete' data-confirm='Are you sure you want to delete this contact'>Delete</a> </td> </tr>";
      table = $('#contact-table');
      if (action_type === 'create') {
        prev_row = table.find("tbody tr").last();
        if (prev_row.length > 0) {
          return $(row).insertAfter(prev_row);
        } else {
          return table.append("<tbody>" + row + "</tbody>");
        }
      } else {
        current_row = table.find("tbody tr#" + contact.id);
        prev_row = current_row.prev();
        current_row.remove();
        if (prev_row.length > 0) {
          return $(row).insertAfter(prev_row);
        } else {
          return table.find('tbody').prepend(row);
        }
      }
    };
    return render_custom_input = function(value) {
      if (value == null) {
        value = false;
      }
      if (value === false) {
        value = '';
      }
      return "<input type='text' name='contact_fields[]' class='form-control' value='" + value + "'>";
    };
  });

}).call(this);

//# sourceMappingURL=app.js.map
