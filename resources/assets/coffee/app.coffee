
$ ->

  $(".btn-save-contact").click ->
    console.log 'save contact'
    $('#form-contact').submit();
    return

  # Force datatype to json
  $.ajaxSetup({
    dataType: 'json'
  })

  # AJAX form handler
  $('.ajax-form').on('ajax:success', (e, data, status, xhr) ->
    console.log 'success'
    console.log data
    $('#contact-modal').modal('hide')
    render_row(data.contact, data.action_type)
    return
  ).on "ajax:error", (e, xhr, status, error) ->
    console.log 'process error'
    response_json = JSON.parse(xhr.responseText)
    FormHelper.showValidationErrors($(".ajax-form"), response_json)
    return

  # Delete Contact
  $(document).on("ajax:success", ".delete-contact", (e, data, status, xhr) ->
    console.log 'success'
    $(this).closest('tr').remove();
    return
  ).on "ajax:error", (e, xhr, status, error) ->
    console.log 'process error'
    return

  # Hide Modal
  $("#contact-modal").on "hidden.bs.modal", (e) ->
    console.log 'remove data'
    modal = $(this)
    modal.removeData('bs.modal')
    modal.find('.modal-body #contact_id').val('')
    modal.find('.modal-body #first_name').val('')
    modal.find('.modal-body #last_name').val('')
    modal.find('.modal-body #email').val('')
    modal.find('.modal-body #phone').val('')
    modal.find('.modal-body .custom-fields-container input').remove()
    modal.find('.modal-body .custom-fields-container').prepend(render_custom_input())
    return

  # Show Modal
  $('#contact-modal').on 'show.bs.modal', (event) ->
    modal = $(this)
    button = $(event.relatedTarget)

    if (button.hasClass('btn-add'))
      modal_title = 'Add Contact'
      modal.find('.modal-body form').attr('method', 'post')
      modal.addClass('add-contact-modal')
    else
      contact_id = button.data('id')
      modal_title = 'Edit Contact'
      modal.find('.modal-body form').attr('method', 'put')
      modal.addClass('edit-contact-modal')

      # Fetch contact and load to modal form inputs
      $.get "/get_contact?id=#{contact_id}", (data) ->
        console.log data
        modal.find('.modal-body #contact_id').val(data.contact.id)
        modal.find('.modal-body #first_name').val(data.contact.first_name)
        modal.find('.modal-body #last_name').val(data.contact.last_name)
        modal.find('.modal-body #email').val(data.contact.email)
        modal.find('.modal-body #phone').val(data.contact.phone)
        if data.contact_fields.length > 0
          custom_inputs = []
          $.each data.contact_fields, (key, value) ->
            custom_inputs.push(render_custom_input(value.name))
          modal.find('.modal-body .custom-fields-container input').remove()
          modal.find('.modal-body .custom-fields-container').prepend(custom_inputs.join(''))

        return

    modal.find('.modal-title').text modal_title

    return

  # Add custom field
  $(".btn-add-field").click ->
    console.log 'render custom field'
    $(render_custom_input()).insertAfter($('.custom-fields-container input').last())
    $('.custom-fields-container input').last().focus()

  # Render Row
  render_row = (contact, action_type) ->
    row = "<tr id='#{contact.id}'>
      <th scope='row'>#{contact.id}</th>
        <td>#{contact.first_name}</td>
        <td>#{contact.last_name}</td>
        <td>#{contact.email}</td>
        <td>#{contact.phone}</td>
        <td>
          <button type='button' class='btn btn-info btn-edit' data-toggle='modal' data-target='#contact-modal' data-id='#{contact.id}'>Edit</button>
          <a href='http://contactbook.dev/contact/#{contact.id}' class='btn btn-danger delete-contact' data-remote='true' data-method='delete' data-confirm='Are you sure you want to delete this contact'>Delete</a>
        </td>
      </tr>"
    table = $('#contact-table')
    if action_type == 'create'
      prev_row = table.find("tbody tr").last()
      if prev_row.length > 0
        $(row).insertAfter(prev_row);
      else
        table.append("<tbody>#{row}</tbody>")
    else
      current_row = table.find("tbody tr##{contact.id}")
      prev_row = current_row.prev()
      # console.log current_row
      # console.log prev_row
      current_row.remove()
      if prev_row.length > 0
        $(row).insertAfter(prev_row);
      else
        table.find('tbody').prepend(row)

  render_custom_input = (value = false) ->
    if value == false
      value = ''
    "<input type='text' name='contact_fields[]' class='form-control' value='#{value}'>"






