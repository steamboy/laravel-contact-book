FormHelper = FormHelper or {}

@FormHelper =
  initialize: ->

  showValidationErrors: (form, obj) ->

    # Remove .has-error
    $('.form-group').removeClass('has-error')
    $('.help-block.has-error').remove()

    # console.log obj
    $.each obj, (id, error_messages) ->
      input_id = id

      # console.log input_id

      # Find label and add .has-error
      label = form.find("label[for='#{input_id}']")

      if !label.parent().hasClass('has-error')
        label.parent().addClass('has-error')

      # console.log "##{input_id}"

      # Find input and move it inside field_with_errors
      input = form.find("##{input_id}")

      # For Radio button
      if label.parent().find('.radio').length > 0
        input = label.parent().find('.radio').last().find('input').parent().parent()

      # For Radio Inlue button
      if label.parent().find('.radio-inline').length > 0
        input = label.parent().find('.radio-inline').last().find('input').parent().parent()

      # Render error message
      $.each error_messages, (index, error_message) ->
        if input.parent().find('span:contains("' + error_message + '")').length is 0
          $('<span for="validation_name" class="help-block has-error">' + error_message + '</span>').insertAfter(input)

    return
