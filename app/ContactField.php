<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactField extends Model
{
    protected $fillable = ['contact_id', 'name'];

    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }
}
