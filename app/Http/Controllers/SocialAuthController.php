<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SocialAccountService;
use Socialite;

class SocialAuthController extends Controller
{
    // Facebook Stuff
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function callback(SocialAccountService $service)
    {
        $user = $service->createOrGetUser('facebook', Socialite::driver('facebook')->user());

        auth()->login($user);

        return redirect()->to('/home');
    }

    // Github Stuff
    public function github_redirect()
    {
        return Socialite::driver('github')->redirect();
    }

    public function github_callback(SocialAccountService $service)
    {
        $user = $service->createOrGetUser('github', Socialite::driver('github')->user());

        auth()->login($user);

        return redirect()->to('/home');
    }
}