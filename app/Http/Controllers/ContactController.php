<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Contact;
use App\Repositories\ContactRepository;
use App\Repositories\ContactFieldRepository;
use ActiveCampaign;

class ContactController extends Controller
{

    protected $contacts;

    public function __construct(ContactRepository $contacts)
    {
        $this->middleware('auth');

        $this->contacts = $contacts;

        $this->active_campaign = new ActiveCampaign(env('ACTIVECAMPAIGN_URL'), env('ACTIVECAMPAIGN_KEY'));
    }

    public function store(Request $request)
    {
        // return response()->json($request->contact_fields);
        // exit;

        $this->validate($request, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email',
            'phone' => 'required|max:255',
        ]);

        $params = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phone' => $request->phone,
        ];

        $contact = $request->user()->contacts()->create($params);
        if ($contact)
        {

            foreach ($request->contact_fields as $contact_field) {
                if ($contact_field != ''){
                    $contact->contact_fields()->create([
                        'name' => $contact_field
                    ]);
                }
            }

            $this->active_campaign_trigger($contact);

            $request = ['status' => 'success', 'action_type' => 'create', 'contact' => $contact, 'contact_fields' => $contact->contact_fields()->select('name')->get()];
        }

        return response($request);
    }

    public function index(Request $request)
    {
        return view('contacts.index', [
            'contacts' => $this->contacts->forUser($request->user()),
        ]);
    }

    public function search(Request $request)
    {
        $contacts = Contact::where('user_id', $request->user())
                    ->orwhere('first_name', 'like', "%$request->q%")
                    ->orWhere('last_name', 'like', "%$request->q%")
                    ->orWhere('email', 'like', "%$request->q%")
                    ->orderBy('created_at', 'asc')
                    ->get();

        return view('contacts.search', [
            'contacts' => $contacts,
        ]);

        // return view('contacts.search', [
        //     'contacts' => $this->contacts->forUser($request->user()),
        // ]);
    }

    public function destroy(Request $request, Contact $contact)
    {
        // $this->authorize('destroy', $contact);

        if ($contact->delete()){
            $request = ['status' => 'success'];
        }

        return response($request);
    }

    public function get_contact(Request $request)
    {
        $contact = Contact::find($request->id);

        $request = ['status' => 'success', 'action_type' => 'create', 'contact' => $contact, 'contact_fields' => $contact->contact_fields()->select('name')->get()];

        return response($request);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email',
            'phone' => 'required|max:255',
        ]);

        $contact = Contact::find($request->id);
        $contact->first_name = $request->first_name;
        $contact->last_name = $request->last_name;
        $contact->email = $request->email;
        $contact->phone = $request->phone;

        if ($contact->save())
        {
            $contact->contact_fields()->delete();
            foreach ($request->contact_fields as $contact_field) {
                if ($contact_field != ''){
                    $contact->contact_fields()->create([
                        'name' => $contact_field
                    ]);
                }
            }

            $this->active_campaign_trigger($contact);

            $request = ['status' => 'success', 'action_type' => 'update', 'contact' => $contact];

        }

        return response($request);
    }

    private function active_campaign_trigger($contact)
    {
        // Custom fields
        // $custom_fields_merged = '';
        // $contact_fields = $contact->contact_fields()->select('name')->get();
        // if (count($contact_fields) > 0) {
        //     $custom_fields = [];
        //     foreach ($contact_fields as $contact_field) {
        //         $custom_fields[] = $contact_field->name;
        //     }
        //     $custom_fields_merged = implode(", ", $custom_fields);
        // }

        // START API
        $list_id = 1; // Default Test List
        $ac_contact = array(
            "email"              => $contact->email,
            "first_name"         => $contact->first_name,
            "last_name"          => $contact->last_name,
            "phone"              => $contact->phone,
            // "custom_fields"      => $custom_fields_merged,
            "p[{$list_id}]"      => $list_id,
            "status[{$list_id}]" => 1, // "Active" status
        );
        $ac_contact_sync = $this->active_campaign->api("contact/sync", $ac_contact);
        if (!(int)$ac_contact_sync->success) {
            // request failed
            // echo "<p>Syncing contact failed. Error returned: " . $ac_contact_sync->error . "</p>";
            // exit();
            return false;
        }
        else {
            // successful request
            $ac_contact_id = (int)$ac_contact_sync->subscriber_id;
            return true;
        }
    }

}
