<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

// Facbook
Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');

// Github
Route::get('/github_redirect', 'SocialAuthController@github_redirect');
Route::get('/github_callback', 'SocialAuthController@github_callback');

// Contacts
Route::get('/home', 'ContactController@index');
Route::post('/contact', 'ContactController@store');
Route::put('/contact', 'ContactController@update');
Route::delete('/contact/{contact}', 'ContactController@destroy');
Route::get('/get_contact', 'ContactController@get_contact');
Route::get('/search', 'ContactController@search');
