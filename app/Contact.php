<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['user_id', 'first_name', 'last_name', 'email', 'phone'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function contact_fields()
    {
        return $this->hasMany(ContactField::class);
    }
}
